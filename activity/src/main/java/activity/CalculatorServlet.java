package activity;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8254685445871886486L;
	private String contentType;
	
	public void init() throws ServletException{
		
		contentType = "index/html";
		System.out.println("*********************************");
		System.out.println(" Initialized connection to DB ");
		System.out.println("*********************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		ServletContext srvContext = getServletContext();
		PrintWriter out = res.getWriter();
		
		String instruction1 = srvContext.getInitParameter("instruction_1");
		String instruction2 = srvContext.getInitParameter("instruction_2");
		String instruction3 = srvContext.getInitParameter("instruction_3");
		
		out.println("<h1>You are now using the calculator app</h1>" + 
		"<p>" + instruction1 + "</p>" +
		"<p>" + instruction2 + "</p>" +
		"<p>" + instruction3 + "</p>");
	}
	
	public void destroy() {
		System.out.println("*********************************");
		System.out.println(" Disconnected from DB ");
		System.out.println("*********************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res)throws IOException{
		System.out.println("Hello from the calculator servlet");
		
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		
		String operation = req.getParameter("operation");
		
		int answer = 0;
		
		switch(operation) {
			case "add":
				answer = num1 + num2;
				break;
			case "subtract":
				answer = num1 - num2;
				break;
			case "multiply":
				answer = num1 * num2;
				break;
			case "divide":
				answer = num1 / num2;
		}
	
		PrintWriter out = res.getWriter();
		out.println("<p>The two numbers you provided are: " + num1 + ", " + num2 + "</p>");
		out.println("<p>The operation that you wanted is: " + operation + "</p>");
		out.println("<p>The total of the two numbers are: " + answer + "</p>");
		
	}
}
